package com.crackthatcd.RestStart.SampleRestFullService.controllers;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.crackthatcd.RestStart.SampleRestFullService.dao.UserRepository;
import com.crackthatcd.RestStart.SampleRestFullService.model.Post;
import com.crackthatcd.RestStart.SampleRestFullService.model.User;

@RestController
public class UserController {

	@Autowired
	UserRepository userRepo;

	@GetMapping("/jpa/users")
	public List<User> getAllUsers() {

		List<User> allUsers = userRepo.findAll();
		return allUsers;

	}

	@GetMapping("/jpa/users/{id}")
	public Optional<User> getUserById(@PathVariable int id) {

		Optional<User> allUsers = userRepo.findById(id);
		return allUsers;

	}

	@DeleteMapping("/jpa/users/{id}")
	public void removeUserById(@PathVariable int id) {

		userRepo.deleteById(id);

	}

	@PostMapping("/jpa/users")
	public ResponseEntity<Object> createUser(@Valid @RequestBody User user) {

		User savedUser = userRepo.save(user);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(savedUser.getId())
				.toUri();

		return ResponseEntity.created(location).build();

	}

	@GetMapping("/jpa/users/{id}/posts")
	public List<Post> getAllPostsForUser(@PathVariable int id) {

		List<Post> allPosts = userRepo.findById(id).get().getPosts();
		return allPosts;

	}

	@PostMapping("/jpa/users/{id}/posts")
	public ResponseEntity<Object> createPost(@PathVariable int id, @Valid @RequestBody Post post) {

		Optional<User> foundUser = userRepo.findById(id);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(foundUser.get().getId()).toUri();

		return ResponseEntity.created(location).build();

	}
}
