package com.crackthatcd.RestStart.SampleRestFullService.controllers;

import java.time.LocalDate;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crackthatcd.RestStart.SampleRestFullService.model.User;

@RestController

public class GenericController {

	@GetMapping("/hello-world")
	public String hello() {
		return "Hello-World";
	}

	@GetMapping("/users")
	public User getUser() {
		User user = new User("Piyush", "Mishra", 28, "Software Developer", LocalDate.of(1990, 01, 01));
		return user;
	}

}
