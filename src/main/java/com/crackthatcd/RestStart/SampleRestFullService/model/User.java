/**
 * 
 */
package com.crackthatcd.RestStart.SampleRestFullService.model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

/**
 * @author Jhakkas
 *
 */

@Entity
public class User {

	@Id
	@GeneratedValue
	int id;

	@Size(min = 2, message = "Fisrt name should be atleast 2 characters long")
	String firstName;
	String lastName;
	int age;
	String position;

	@Past
	LocalDate dob;

	@OneToMany(mappedBy = "user")
	private List<Post> posts;

	public User(String firstName, String lastName, int age, String position, LocalDate dob) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.position = position;
		this.dob = dob;
	}

	public User() {

	}

	public List<Post> getPosts() {
		return posts;
	}

	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}

	public String getFirstName() {
		return firstName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public LocalDate getDob() {
		return dob;
	}

	public void setDob(LocalDate dob) {
		this.dob = dob;
	}

}
