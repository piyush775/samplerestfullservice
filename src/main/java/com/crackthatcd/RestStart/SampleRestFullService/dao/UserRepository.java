package com.crackthatcd.RestStart.SampleRestFullService.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.crackthatcd.RestStart.SampleRestFullService.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

}
