package com.crackthatcd.RestStart.SampleRestFullService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleRestFullServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SampleRestFullServiceApplication.class, args);
	}
}
